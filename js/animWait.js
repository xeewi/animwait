/* 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Waiting Page Animations Script

Created by.
 ______     ______     __  __     ______   ______     ______     __    __    
/\  == \   /\  __ \   /\_\_\_\   /\__  _\ /\  ___\   /\  __ \   /\ "-./  \   
\ \  __<   \ \ \/\ \  \/_/\_\/_  \/_/\ \/ \ \  __\   \ \  __ \  \ \ \-./\ \  
 \ \_\ \_\  \ \_____\   /\_\/\_\    \ \_\  \ \_____\  \ \_\ \_\  \ \_\ \ \_\ 
  \/_/ /_/   \/_____/   \/_/\/_/     \/_/   \/_____/   \/_/\/_/   \/_/  \/_/ 

  -- > Xeewi
                                                                             
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/

/*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
						AnimWait
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// FUNCTIONS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

/* ------------------------------
--- From / To
------------------------------ */

// Top
// ********************
function awToTop(block){
	$(block.id).css('top', '0').animate({'top': '-100%'}, 600);
	setTimeout(function() {$(block.id).hide();}, 600);
	$(block.id).animate({'top' : '0'},1);
}
function awFromTop(block){
	$(block.id).show().css('top', '-100%').animate({'top': '0'}, 600);
}

// Bot
// ********************
function awToBot(block){
	$(block.id).css('top', '0').animate({'top': '100%'}, 600);
	setTimeout(function() {$(block.id).hide();}, 600);
	$(block.id).animate({'top' : '0'},1);
}
function awFromBot(block){
	$(block.id).show().css('top', '100%').animate({'top': '0'}, 600);
}

// Left
// ********************
function awToLeft(block){
	$(block.id).css('left', '0').animate({'left': '-100%'}, 600);
	setTimeout(function() {$(block.id).hide();}, 600);
	$(block.id).animate({'left' : '0'},1);
}
function awFromLeft(block){
	$(block.id).show().css('left', '-100%').animate({'left': '0'}, 600);
}

// Right
// ********************
function awToRight(block){
	$(block.id).css('left', '0').animate({'left': '100%'}, 600);
	setTimeout(function() {$(block.id).hide();}, 600);
	$(block.id).animate({'left' : '0'},1);
}
function awFromRight(block){
	$(block.id).show().css('left', '100%').animate({'left': '0'}, 600);
}


// F/T Selector
// ********************
function awFromSelector(config, number){
	if (number == "one") {block = config.one};
	if (number == "two") {block = config.two};
	if (number == "three") {block = config.three};

	if (block.from == "Top" || 
		block.from == "Bot" || 
		block.from == "Left" || 
		block.from == "Right") {
		animFt = "from" + block.from;
	} else {
		animFt = block.from;
	}

	return animFt;
}

function awToSelector(config, number) {
	if (number == "one") {block = config.one};
	if (number == "two") {block = config.two};
	if (number == "three") {block = config.three};

	if (block.to == "Top" || 
		block.to == "Bot" || 
		block.to == "Left" || 
		block.to == "Right") {
		animFt = "to" + block.to;
	} else {
		animFt = block.to;
	}

	return animFt;
}

// Revert
function awFromSelectorRevert(config, number){
	if (number == "one") {block = config.one};
	if (number == "two") {block = config.two};
	if (number == "three") {block = config.three};

	if (block.from == "Top" || 
		block.from == "Bot" || 
		block.from == "Left" || 
		block.from == "Right") { animFt = "to" + block.from;}; 
	if (block.from == "fadeIn") {animFt = "fadeOut"; };
	if (block.from == "show") {animFt = "hide"; };

	return animFt;
}
function awToSelectorRevert(config, number){
	if (number == "one") {block = config.one};
	if (number == "two") {block = config.two};
	if (number == "three") {block = config.three};

	if (block.from == "Top" || 
		block.from == "Bot" || 
		block.from == "Left" || 
		block.from == "Right") { animFt = "from" + block.from;}; 
	if (block.from == "fadeOut") {animFt = "fadeIn"; };
	if (block.from == "hide") {animFt = "show"; };

	return animFt;
}

// --> FROM / TO
// ~~~~~~~~~~~~~~~~~~
function awFromTo(config, number, anim){

	if (number == "one") {block = config.one};
	if (number == "two") {block = config.two};
	if (number == "three") {block = config.three};

	if (anim == "fromTop") { awFromTop(block); };
	if (anim == "fromBot") { awFromBot(block); };
	if (anim == "fromLeft") { awFromLeft(block); };
	if (anim == "fromRight") { awFromRight(block); };

	if (anim == "toTop") { awToTop(block); };
	if (anim == "toBot") { awToBot(block); };
	if (anim == "toLeft") { awToLeft(block); };
	if (anim == "toRight") { awToRight(block); };

	if (anim == "hide") { $(block.id).hide()};
	if (anim == "show") { $(block.id).show()};
	if (anim == "fadeIn") { $(block.id).fadeIn(800)};
	if (anim == "fadeOut") { $(block.id).fadeOut(800)};
}

/* ------------------------------
--- Transitions
------------------------------ */

// Horizontal
// ********************
// Open
function awHorizontalOpen( config , flap , number ){
	if (number == "one") {
		block = config.one;
	};
	if (number == "two") {
		block = config.two;
	};
	if (number == "three") {
		block = config.three;
	};

	$(flap.one).show().css({
		'z-index' : '11',
		'top' : '0',
		'left': '0',
		'width' : '50%',
		'height' : '100%'});
	$(flap.two).show().css({
		'z-index' : '11',
		'top' : '0',
		'left': '50%',
		'width' : '50%',
		'height' : '100%'});

	fromto = awFromSelector(config, number);

	setTimeout(function(){
		$(flap.one).animate({'left' : '-50%'}, 1000);
		$(flap.two).animate({'left' : '100%'}, 1000);
	} , block.delay+500);

	$(block.id).css('z-index' , '10');

	setTimeout(function(){awFromTo(config, number, fromto);}, block.delay+500);
	setTimeout(function(){ $(flap.one).hide(); $(flap.two).hide(); }, block.delay+1500)
}
// Close
function awHorizontalClose( config , flap , number ){
	if (number == "one") {
		block = config.one;
	};
	if (number == "two") {
		block = config.two;
	};
	if (number == "three") {
		block = config.three;
	};

	$(flap.one).show().css({
		'z-index' : '11',
		'top' : '0',
		'left': '-50%',
		'width' : '50%',
		'height' : '100%'});
	$(flap.two).show().css({
		'z-index' : '11',
		'top' : '0',
		'left': '100%',
		'width' : '50%',
		'height' : '100%'});

	fromto = awFromSelector(config, number);

	console.log(fromto);

	setTimeout(function(){
		$(flap.one).animate({'left' : '0%'}, 1000);
		$(flap.two).animate({'left' : '50%'}, 1000);
	} , 0);

	$(block.id).css('z-index' , '12');

	setTimeout(function(){awFromTo(config, number, fromto);}, block.delay+200);
}

// Vertical
// ********************
// Open
function awVerticalOpen( config , flap , number ){
	if (number == "one") {
	block = config.one;
	};
	if (number == "two") {
		block = config.two;
	};
	if (number == "three") {
		block = config.three;
	};

	$(flap.one).show().css({
		'z-index' : '11',
		'top': '0',
		'left' : '0',
		'width' : '100%',
		'height' : '50%'});
	$(flap.two).show().css({
		'z-index' : '11',
		'top': '50%',
		'left' : '0',
		'width' : '100%',
		'height' : '50%'});

	fromto = awFromSelector(config, number);

	setTimeout(function(){
		$(flap.one).animate({'top' : '-50%'}, 1000);
		$(flap.two).animate({'top' : '100%'}, 1000);
	} , block.delay+500);

	$(block.id).css('z-index' , '10');

	setTimeout(function(){awFromTo(config, number, fromto);}, block.delay+500);
	setTimeout(function(){ $(flap.one).hide(); $(flap.two).hide(); }, block.delay+1500)
}
// Close
function awVerticalClose( config , flap , number ){
	if (number == "one") {
	block = config.one;
	};
	if (number == "two") {
		block = config.two;
	};
	if (number == "three") {
		block = config.three;
	};

	$(flap.one).show().css({
		'z-index' : '11',
		'top': '-50%',
		'left' : '0',
		'width' : '100%',
		'height' : '50%'});
	$(flap.two).show().css({
		'z-index' : '11',
		'top': '100%',
		'left' : '0',
		'width' : '100%',
		'height' : '50%'});


	fromto = awFromSelector(config, number);

	setTimeout(function(){
		$(flap.one).animate({'top' : '0%'}, 1000);
		$(flap.two).animate({'top' : '50%'}, 1000);
	} , 0);

	$(block.id).css('z-index' , '12');

	setTimeout(function(){awFromTo(config, number, fromto);}, block.delay+200);
}

// --> TRANSITIONS
// ~~~~~~~~~~~~~~~~~~
function awTransitions( config , flap , number ) {

	if (config.beginning == "open") {
		beginning = { "one" : "open" , "two" : "close" , "three" : "open"};
	} else if (config.beginning == "close") {
		beginning = { "one" : "close" , "two" : "open" , "three" : "close"};
	}

	if (number == "one") { block = config.one; };
	if (number == "two") { block = config.two; };
	if (number == "three") { block = config.three; };

	if (block.transition == "vertical") {
		if (number == "one") {
			if (beginning.one == "open") {
				awVerticalOpen(config , flap , number);
			} else if (beginning.one == "close") {
				awVerticalClose(config , flap , number);
			};
		} else if (number == "two") {
			if (beginning.two == "open") {
				awVerticalOpen(config , flap , number);
			} else if (beginning.two == "close") {
				awVerticalClose(config , flap , number);
			};
		} else if (number == "three") {
			if (beginning.three == "open") {
				awVerticalOpen(config , flap , number);
			} else if (beginning.three == "close") {
				awVerticalClose(config , flap , number);
			};
		};
	} else if (block.transition == "horizontal") {
		if (number == "one") {
			if (beginning.one == "open") {
				awHorizontalOpen(config , flap , number);
			} else if (beginning.one == "close") {
				awHorizontalClose(config , flap , number);
			};
		} else if (number == "two") {
			if (beginning.two == "open") {
				awHorizontalOpen(config , flap , number);
			} else if (beginning.two == "close") {
				awHorizontalClose(config , flap , number);
			};
		} else if (number == "three") {
			if (beginning.three == "open") {
				awHorizontalOpen(config , flap , number);
			} else if (beginning.three == "close") {
				awHorizontalClose(config , flap , number);
			};
		};
	};
}

// revert 
function awTransitionsRevert( config , flap , number ) {

	if (config.beginning == "open") {
		beginning = { "one" : "close" , "two" : "open" , "three" : "close"};
	} else if (config.beginning == "close") {
		beginning = { "one" : "open" , "two" : "close" , "three" : "open"};
	}

	if (number == "one") { block = config.one; };
	if (number == "two") { block = config.two; num = "one"; };
	if (number == "three") { block = config.three; num = "two"; };

	if (block.transition == "vertical") {
		if (number == "one") {
			if (beginning.one == "open") {
				awVerticalOpen(config , flap , num);
			} else if (beginning.one == "close") {
				awVerticalClose(config , flap , num);
			};
		} else if (number == "two") {
			if (beginning.two == "open") {
				awVerticalOpen(config , flap , num);
			} else if (beginning.two == "close") {
				awVerticalClose(config , flap , num);
			};
		} else if (number == "three") {
			if (beginning.three == "open") {
				awVerticalOpen(config , flap , num);
			} else if (beginning.three == "close") {
				awVerticalClose(config , flap , num);
			};
		};
	} else if (block.transition == "horizontal") {
		if (number == "one") {
			if (beginning.one == "open") {
				awHorizontalOpen(config , flap , num);
			} else if (beginning.one == "close") {
				awHorizontalClose(config , flap , num);
			};
		} else if (number == "two") {
			if (beginning.two == "open") {
				awHorizontalOpen(config , flap , num);
			} else if (beginning.two == "close") {
				awHorizontalClose(config , flap , num);
			};
		} else if (number == "three") {
			if (beginning.three == "open") {
				awHorizontalOpen(config , flap , num);
			} else if (beginning.three == "close") {
				awHorizontalClose(config , flap , num);
			};
		};
	};
}

/* ------------------------------
--- Position / Animations
------------------------------ */

// Element Vertical Center
// ***************
function awVerticalCenter(id){
	// id -> string
	var htuser  = $(window).height();
	var htelem  = $(id).height();
	var margtop = (htuser - htelem)/2;

	$(id).css('margin-top', margtop);

};

// 360° rotation
// ***************
function awRotate(load){
	var degrees = 0;
	var vrotate = 5; // vitesse de rotation

	var interval = setInterval(function(){
		$(load.img).css({
			'-webkit-transform' : 'rotate('+ degrees +'deg)',
			'-moz-transform' : 'rotate('+ degrees +'deg)',
			'-ms-transform' : 'rotate('+ degrees +'deg)',
			'transform' : 'rotate('+ degrees +'deg)'
		});
		degrees++;
	}, vrotate);

	setTimeout(function(){clearInterval(interval)}, load.timer); // Durée rotation
};

/* ------------------------------
--- Loading
------------------------------ */
function awLoad(load){
	awVerticalCenter(load.img);
	awRotate(load);
	$(load.img).delay(load.timer).fadeOut(load.timer);
	$(load.id).delay(load.timer).fadeOut('400');
}

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// AnimWait
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

function AnimWait( config ){

	if (typeof(config) == "undefined") {
		var config = { 

		beginning : "open",

			one : {
				from : "Top",
				to : "Bot",
				transition : "horizontal",
				delay : 1000
			},

			two : {
				from : "Left",
				to : "Right",
				transition : "vertical",
				delay : 500
			},

			three : {
				from : "fadeIn",
				transition : "horizontal",
				delay : 500
			}
		}
	};

	// Recovry
	config.one.id   = "#aw-block-one";
	config.two.id   = "#aw-block-two";
	config.three.id = "#aw-block-three";

	var load = { id : "#aw-load" , timer : config.one.delay , img : "#aw-load-img"};
	var flap = { one : "#aw-flap-one" , two : "#aw-flap-two"};
	var bouton = { one : "#aw-bouton-one" , two : "#aw-bouton-two" };
	var breturn = { one : "#aw-return-one" , two : "#aw-return-two" };

	// Loading & Transition One
	awLoad(load);
	awTransitions(config, flap , "one");

	// Boutons
	$(bouton.one).click(function() {
		var anim = awToSelector(config, "one");
		awFromTo(config, "one", anim);
		setTimeout(function() {awTransitions(config , flap , "two")}, config.two.delay);
	});

	$(bouton.two).click(function() {
		var anim = awToSelector(config, "two");
		awFromTo(config, "two", anim);
		setTimeout(function() {awTransitions(config , flap , "three")}, config.three.delay);
	});

	// Returns
	$(breturn.one).click(function() {
		var anim = awFromSelectorRevert(config, "two");
		awFromTo(config , "two" , anim);
		setTimeout(function() {awTransitionsRevert(config , flap , "two")}, 0);
	});

	$(breturn.two).click(function() {
		var anim = awFromSelectorRevert(config, "three");
		awFromTo(config , "three" , anim);
		setTimeout(function() {awTransitionsRevert(config , flap , "three")}, 0);
	});
}