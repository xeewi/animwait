<!--
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 ______     ______     __  __     ______   ______     ______     __    __    
/\  == \   /\  __ \   /\_\_\_\   /\__  _\ /\  ___\   /\  __ \   /\ "-./  \   
\ \  __<   \ \ \/\ \  \/_/\_\/_  \/_/\ \/ \ \  __\   \ \  __ \  \ \ \-./\ \  
 \ \_\ \_\  \ \_____\   /\_\/\_\    \ \_\  \ \_____\  \ \_\ \_\  \ \_\ \ \_\ 
  \/_/ /_/   \/_____/   \/_/\/_/     \/_/   \/_____/   \/_/\/_/   \/_/  \/_/ 

	AnimWait - index.php
	
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-->

<!DOCTYPE html>
<html>
<head>
	<title>AnimWait - By Xeewi</title>
	<meta charset="utf-8" />
	<meta description="AnimWait is an animate HTML page, using jQuery. - By. Xeewi">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<meta property="og:title" content="AnimWait - by Xeewi" />
	<meta property="og:description" content="Animate waiting page template using jQuery" />
	<meta property="og:url" content="http://gautierguillaume.com/projects/animwait/" />
</head>
<body>

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//	LOADING
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

	<div id="aw-load">
		<img src="img/load.png" class="aw-img" id="aw-load-img" alt="AltText">
	</div>

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//	FLAPS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

	<!-- Flap 1
	~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
	<div id="aw-flap-one"></div>
	<!-- end Flap 1 ~~~~~~~~~~~~ -->

	<!-- Flap 2
	~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
	<div id="aw-flap-two"></div>
	<!-- end Flap 2 ~~~~~~~~~~~~ -->

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//	BLOCKS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

	<!-- Block 1 
	~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
	<div id="aw-block-one">
	<div class="aw-container">
		<h1>AnimWait</h1>
		<img src="img/logo.jpg" class="aw-img" alt="AltText">
		<p>
			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut sed consectetur urna, quis scelerisque odio. Fusce enim quam, porttitor vel metus vitae, rutrum dictum odio. Praesent hendrerit sem vitae augue aliquet tristique. Aliquam erat volutpat. Etiam bibendum massa non faucibus accumsan. Quisque nec semper nibh, a pharetra nisi. Nullam faucibus et justo et condimentum. Suspendisse convallis arcu vel pellentesque vehicula. Duis eu rhoncus est. Vestibulum quis mauris tortor. In felis nibh, laoreet at felis eget, dignissim molestie lectus. Suspendisse vitae quam nunc. Nullam sit amet tortor malesuada, lacinia felis convallis, aliquet ex. Donec gravida enim vel sem blandit, sed semper ligula euismod. 
		</p>
		<p class="aw-center">
			<span id="aw-bouton-one">Suivant</span>
		</p>
	</div>
	</div>
	<!-- end Block 1 ~~~~~~~~~~~~ -->

	<!-- Block 2
	~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
	<div id="aw-block-two">
	<div class="aw-container">
		<span id="aw-return-one">Précédent</span>
		<h2>Secondary Title</h2>
		<p>
			Nullam viverra, massa ac tristique ullamcorper, magna diam egestas lacus, eget ultricies mi justo quis orci. Duis mattis mauris sit amet massa condimentum pharetra. Aenean imperdiet posuere malesuada. Morbi eleifend dolor odio, sit amet vestibulum odio ultrices fermentum. In in congue urna. Vivamus a faucibus sapien, eu elementum lorem. Phasellus bibendum id augue eu laoreet. In finibus sem eu nunc interdum, a suscipit purus interdum.
		</p>
		<img src="img/start-up-1-pixabay.jpg" class="aw-img aw-center" alt="AltText"/>
		<p class="aw-center">
			<span id="aw-bouton-two">Suivant</span>
		</p>
	</div>
	</div>
	<!-- end Block 2 ~~~~~~~~~~~~ -->

	<!-- Block 3
	~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
	<div id="aw-block-three">
	<div class="aw-container">
		<span id="aw-return-two">Précédent</span>
		<h2>Thanks for watching</h2>
		<p>You can download the .rar and read the documentation :)</p>
		<p class="aw-center"><a class="bouton" href="animwait.rar">Download</a></p>
		<p class="aw-center"><a class="bouton" href="doc/index.html">Documentation</a></p>
		<p class="aw-center"><a class="bouton" href="https://bitbucket.org/xeewi/animwait">View on BitBucket</a></p>

		<p class="aw-center">Created by. <a href="http://gautierguillaume.com">Xeewi</a></p>
	</div>
	<!-- end Block 3 ~~~~~~~~~~~~ -->

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//	SCRIPTS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

	<script type="text/javascript" src="js/jquery.js"></script> <!-- jQuery -->
	<script type="text/javascript" src="js/animWait.min.js"></script> <!-- AnimWait -->
	<script type="text/javascript">
	var awConfig = { 

		beginning : "open",

		one : {
			from : "Top",
			to : "Bot",
			transition : "vertical",
			delay : 1000
		},

		two : {
			from : "Left",
			to : "Right",
			transition : "vertical",
			delay : 500
		},

		three : {
			from : "fadeIn",
			transition : "horizontal",
			delay : 500
		}
	}

	AnimWait(awConfig);
	</script>

</body>
</html>