# AnimWait -- School Project

AnimWait is an **animate HTML page**, using **jQuery**.

It's divided into **three parts** plus a loading width **3 transitions**, based on a flap system, between each parts.

For more informations, watch the [documentation](http://gautierguillaume.com/projects/AnimWait/doc/).